# Projet d'Intelligence Artificielle : Jeu Vidéo

### Pierre Lacouve et Jules Borgard
##### 16/11/2019

Sommaire
===
* Introduction
* Principe
    1. Le jeu vidéo et ses règles
    1. Le réseau neuronal et sa formation
* Conclusion

Introduction
===
Ce projet est effectué dans le cadre de la formation Master 1ère année d'informatique à l'ISIMA.
Le but de cet exercice est de nous faire utiliser une forme d'intellignce artificielle intégrée dans un projet.
Nous avons choisi de créer un réseau neuronal comme adversaire d'un jeu vidéo.
Dans ce document, vous retrouverez donc les règles régissant ce jeu ainsi que la formation prévue du réseau neuronal.

Principe
===
1. Le jeu vidéo et ses règles
    ---
    Dans ce jeu, vous êtes une cellule cherchant à proliférer.
    Vous vous trouvez sur le même terrain qu'une autre cellule cherchant la même chose que vous, avec les mêmes armes que vous.
    1. Principes de base
        --- 
        1. Le plateau
            ---
            Tout d'abord, le plateau se présente comme suit, une surface en deux dimentions composées d'alvéoles.
            Chacun de ces hexagones est une place sur laquelle un élément peut se trouver.
            Le "monde" que représente cette surface boucle sur elle-même.
            C'est à dire que la toute dèrnière rangée d'hexagones de droite est reliée à la toute première rangée de gauche, et de même pour celles du haut et du bas de l'écran.
            En somme, lorsqu'un item quelconque se déplacera jusqu'à une des extrémités de l'écran, il "réaparaitra" du côté opposé.
            
        1. Les joueurs
            ---
            La partie s'effectue, à la manière des jeux de plateaux comme les dames, les échecs, ou le jeu de go, c'est à dire que deux joueurs s'affrontent avec les unités qui leur sont fournies en début de partie ainsi que celles qu'ils pourront récolter durant celle-ci.
            Tour a tour, les joueurs auront le droit à une action parmis celles disponibles, comme se déplacer, attaquer, ou se multiplier.
            
        1. Les unités
            ---
            Les unités disponibles aux joueurs sont les suivantes :
            - Les cellules mères : sont les cellules pouvant en générer d'autres sans se multiplier.
            - Les cellules filles : sont les cellules générées par une cellule mère.
                - Les attaquantes : ont une vitesse de déplacement et une défense moyenne, mais effectue de forts dégâts.
                - Les défenseuses : ont une vitesse de déplacement et une attaque moyenne, mais une forte résistance aux dégâts.
                - Les ouvrières : ont une force d'attaque et de défense moyenne mais peuvent se déplacer plus vite.
    
            Ces cellules sont contrôlées par les joueurs du camp auxquelles elles appartiennent.
            Un autre type de cellule, non contrôlé par les joueurs, apparaîtra à intervalles aléatoires et à un quelconque espace libre sur le plateau.
            Ce type de cellule est neutre et constitue un apport en énergie et en masse pour les cellules (cf les caractéristiques). 
    
    1. Le jeu
        ---   
        
        1. Les actions disponibles
            ---
            A tour de rôle, comme expliqué plus haut, chaque joueur aura droit à une action parmis celles listées plus bas.
            Certaines de ces actions ne sont effectuables que par un type spécifique de cellule.
            - Déplacement : s'effectue d'un nombre de cases, et dépends de la vitesse de la cellule.
            - Se multiplier : nécessite au moins une case libre adjascente à celle de la cellule effectuant cette action.
                - Division en 2
                - Génération (Cellules mères uniquement)
            - Attaquer (Cellules filles uniquement) : nécessite une proximité immédiate entre des cellules adverses.
                - Phagociter (manger) : immobilise la cellule attaquante pendant un tour, peut s'effectuer sur une cellule neutre pour en récupérer l'énergie et la masse sans perte. Elle peut aussi être effectué contre une cellule non neutre, une partie de l'énergie et de la masse de la cellule adverse phagocitée est alors perdue.
                - Blesser : attaque simple sur une cellule adversaire adjascente.
            - Transfert d'énergie : deux cellules alliées, peuvent, lorsque placées sur des cases adjascentes, se transférer de l'énergie sans aucune perte.
            
            Les actions comme présentées plus haut auront des résultats différents en fonction de la cellule effectuant cette action, et, pour les actions ciblant une autre cellule, les résultats dépendront aussi de l'état de la cellule subissant l'action ainsi que son camp (allié, enemie ou neutre).
            
        1. Les données des cellules
            ---
            Chaque cellule comporte plusieurs caractéristiques propres pouvant évoluer tout au long du jeu. 
            Ces caractéristques dépendent directement du type de la cellule, de ses dernières actions ainsi que des évènement extérieurs l'ayant précédement affecté.
            - Ressources
                - Masse
                - Energie
            - Caractéristiques
                - Vie
            - Statistiques
                - Vitesse
                - Défense
                - Attaque
        
        1. Le début de partie
            ---
            Les deux joueurs commencent donc avec une cellule mère chacun, avec la même vie, la même masse, énergie etc.
            Les deux cellules sont alors éloignée le plus possible l'une de l'autre, en tenant compte du caractère "bouclé" du plateau.
            Un des deux joueurs choisis donc sa première action comme énoncé plus haut, avant de laisser jouer son adversaire.
            
        1. La fin de partie
            ---
            La partie se termine lorsqu'un des deux joueurs n'a plus de cellule de son camp sur le plateau ou lorsqu'un des deux joueur déclare forfait.

1. Le réseau neuronal
    ---
    Dans cet exercice, nous tenterons de créer un réseau neuronal, et, afin d'obtenir un adversaire intéressant, nous tenterons de le faire jouer à ce jeu en ayant comme unique informations les rêgles, contre une instance de lui même à l'instar d'AlphaZero apprenant à jouer aux échecs de la sorte.
            